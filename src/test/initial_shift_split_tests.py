# -*- mode:python; coding:utf-8; tab-width:4 -*-

from unittest import TestCase

from hamcrest import assert_that, is_

from matrix_utils import matrix_split_and_shift
import Cannon
from common import M1, M2, M3, M4, MX


class MatrixInitialShiftSplitTests(TestCase):
    def test_processors_2x2_block_split(self):
        # given
        NUM_PROCESADORES = 2
        
        A = M2(1, 2,
               3, 4)
        B = M2(5, 6,
               7, 8)
        
        dic = { 0: ( M1(1), M1(5) ),
                1: ( M1(2), M1(8) ),
                2: ( M1(4), M1(7) ),
                3: ( M1(3), M1(6) ) }

        # when
        for id, partial_a, partial_b in matrix_split_and_shift( A, B, NUM_PROCESADORES ):
            # then
            expected_a, expected_b = dic[id]
            assert_that( partial_a, is_( expected_a ) )
            assert_that( partial_b, is_( expected_b ) )


    def test_processors_2x2_block_split2(self):
        # given
        NUM_PROCESADORES = 2
        
        A = MX( 3, 1 )
        B = MX( 3, 3**2 +1 )
        
        dic = { 0: ( M2( 1, 2, 4, 5 ), M2( 10, 11, 13, 14 ) ),
                1: ( M2( 3, 0, 6, 0 ), M2( 18, 0, 0, 0 ) ),
                2: ( M2( 9, 0, 0, 0 ), M2( 16, 17, 0, 0 ) ),
                3: ( M2( 7, 8, 0, 0 ), M2( 12, 0, 15, 0 ) ) }

        # when
        for id, partial_a, partial_b in matrix_split_and_shift( A, B, NUM_PROCESADORES ):
            # then
            expected_a, expected_b = dic[id]
            assert_that( partial_a, is_( expected_a ) )
            assert_that( partial_b, is_( expected_b ) )

    def test_processors_3x3_block_split(self):
        # given
        NUM_PROCESADORES = 3
        
        A = MX( 3, 1 )
        B = MX( 3, 3**2 +1 )
        
        dic = { 0: ( M1(1), M1(10) ),
                1: ( M1(2), M1(14) ),
                2: ( M1(3), M1(18) ),
                3: ( M1(5), M1(13) ),
                4: ( M1(6), M1(17) ),
                5: ( M1(4), M1(12) ),
                6: ( M1(9), M1(16) ),
                7: ( M1(7), M1(11) ),
                8: ( M1(8), M1(15) ) }

        # when
        for id, partial_a, partial_b in matrix_split_and_shift( A, B, NUM_PROCESADORES ):
            # then
            expected_a, expected_b = dic[id]
            assert_that( partial_a, is_( expected_a ) )
            assert_that( partial_b, is_( expected_b ) )

    def test_processors_4x4_block_split(self):
        # given
        NUM_PROCESADORES = 2
        
        A = MX( 4, 1 )
        B = MX( 4, 4**2 +1 )
        
        dic = { 0: ( M2( 1, 2, 5, 6 ), M2( 17, 18, 21, 22 ) ),
                1: ( M2( 3, 4, 7, 8 ), M2( 27, 28, 31, 32 ) ),
                2: ( M2( 11, 12, 15, 16 ), M2( 25, 26, 29, 30 ) ),
                3: ( M2( 9, 10, 13, 14 ), M2( 19, 20, 23, 24 ) ) }

        # when
        for id, partial_a, partial_b in matrix_split_and_shift( A, B, NUM_PROCESADORES ):
            # then
            expected_a, expected_b = dic[id]
            assert_that( partial_a, is_( expected_a ) )
            assert_that( partial_b, is_( expected_b ) )

