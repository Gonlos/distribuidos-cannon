# -*- mode:python; coding:utf-8; tab-width:4 -*-

from unittest import TestCase

from hamcrest import assert_that, anything
from doublex import Spy, called, ANY_ARG

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))
import Cannon

from frontend import FrontendI

from common import M1, M2, M3, M4

class FrontendServantTests(TestCase):
    """
    These are NOT remote tests. We directly instantiate servants here.
    """
    def test_processor_init_2x2_operands_in_2x2_processors(self):
        # given
        ncolsprocs = 2
        frontend = FrontendI(ncolsprocs)
        procs = [Spy(Cannon.Processor) for i in range(ncolsprocs**2)]
        frontend.processors = procs

        # when
        frontend.init_processors_ring()

        # then
        assert_that(procs[0].setProcessors, called().with_args( procs[1], procs[2] ))
        assert_that(procs[1].setProcessors, called().with_args( procs[0], procs[3] ))
        assert_that(procs[2].setProcessors, called().with_args( procs[3], procs[0] ))
        assert_that(procs[3].setProcessors, called().with_args( procs[2], procs[1] ))

    def test_processor_init_3x3_operands_in_3x3_processors(self):
        # given
        ncolsprocs = 3
        frontend = FrontendI(ncolsprocs)
        procs = [Spy(Cannon.Processor) for i in range(ncolsprocs**2)]
        frontend.processors = procs

        # when
        frontend.init_processors_ring()

        # then
        assert_that(procs[0].setProcessors, called().with_args( procs[2], procs[6] ))
        assert_that(procs[1].setProcessors, called().with_args( procs[0], procs[7] ))
        assert_that(procs[2].setProcessors, called().with_args( procs[1], procs[8] ))
        assert_that(procs[3].setProcessors, called().with_args( procs[5], procs[0] ))
        assert_that(procs[4].setProcessors, called().with_args( procs[3], procs[1] ))
        assert_that(procs[5].setProcessors, called().with_args( procs[4], procs[2] ))
        assert_that(procs[6].setProcessors, called().with_args( procs[8], procs[3] ))
        assert_that(procs[7].setProcessors, called().with_args( procs[6], procs[4] ))
        assert_that(procs[8].setProcessors, called().with_args( procs[7], procs[5] ))

    def test_processors_2x2_block_frontend_init(self):
        # given
        NUM_PROCESADORES = 2
        
        processors = []
        for i in xrange( NUM_PROCESADORES**2 ):
            processors.append( Spy( Cannon.Processor ) )
        
        FE = FrontendI( NUM_PROCESADORES )
        FE.processors = processors 

        collector = Spy( Cannon.Collector )
        FE.collector = collector

        A = M2(1, 2,
               3, 4)
        B = M2(5, 6,
               7, 8)

        # when
        FE.init_processors_ring()
        FE.load_processors( A, B )
        
        # then
        assert_that(processors[0].injectA, called().async(timeout=1).with_args(M1(1), ANY_ARG))
        assert_that(processors[1].injectA, called().async(timeout=1).with_args(M1(2), ANY_ARG))
        assert_that(processors[2].injectA, called().async(timeout=1).with_args(M1(4), ANY_ARG))
        assert_that(processors[3].injectA, called().async(timeout=1).with_args(M1(3), ANY_ARG))

        assert_that(processors[0].injectB, called().async(timeout=1).with_args(M1(5), ANY_ARG))
        assert_that(processors[1].injectB, called().async(timeout=1).with_args(M1(8), ANY_ARG))
        assert_that(processors[2].injectB, called().async(timeout=1).with_args(M1(7), ANY_ARG))
        assert_that(processors[3].injectB, called().async(timeout=1).with_args(M1(6), ANY_ARG))

    def test_processors_3x3_block_frontend_init(self):
        # given
        NUM_PROCESADORES = 3
        
        processors = []
        for i in xrange( NUM_PROCESADORES**2 ):
            processors.append( Spy() )
        
        FE = FrontendI( NUM_PROCESADORES )
        FE.processors = processors

        collector = Spy()
        FE.collector = collector

        A = M3(0, 1, 2,
               3, 4, 5,
               6, 7, 8)
        B = M3(9, 10, 11,
               12, 13, 14,
               15, 16, 17)

        # when
        FE.init_processors_ring()
        FE.load_processors( A, B )
        
        # then
        assert_that(processors[0].injectA, called().async(timeout=1).with_args(M1(0), ANY_ARG))
        assert_that(processors[1].injectA, called().async(timeout=1).with_args(M1(1), ANY_ARG))
        assert_that(processors[2].injectA, called().async(timeout=1).with_args(M1(2), ANY_ARG))
        assert_that(processors[3].injectA, called().async(timeout=1).with_args(M1(4), ANY_ARG))
        assert_that(processors[4].injectA, called().async(timeout=1).with_args(M1(5), ANY_ARG))
        assert_that(processors[5].injectA, called().async(timeout=1).with_args(M1(3), ANY_ARG))
        assert_that(processors[6].injectA, called().async(timeout=1).with_args(M1(8), ANY_ARG))
        assert_that(processors[7].injectA, called().async(timeout=1).with_args(M1(6), ANY_ARG))
        assert_that(processors[8].injectA, called().async(timeout=1).with_args(M1(7), ANY_ARG))

        assert_that(processors[0].injectB, called().async(timeout=1).with_args(M1(9), ANY_ARG))
        assert_that(processors[1].injectB, called().async(timeout=1).with_args(M1(13), ANY_ARG))
        assert_that(processors[2].injectB, called().async(timeout=1).with_args(M1(17), ANY_ARG))
        assert_that(processors[3].injectB, called().async(timeout=1).with_args(M1(12), ANY_ARG))
        assert_that(processors[4].injectB, called().async(timeout=1).with_args(M1(16), ANY_ARG))
        assert_that(processors[5].injectB, called().async(timeout=1).with_args(M1(11), ANY_ARG))
        assert_that(processors[6].injectB, called().async(timeout=1).with_args(M1(15), ANY_ARG))
        assert_that(processors[7].injectB, called().async(timeout=1).with_args(M1(10), ANY_ARG))
        assert_that(processors[8].injectB, called().async(timeout=1).with_args(M1(14), ANY_ARG))

