# -*- mode:python; coding:utf-8; tab-width:4 -*-

from doublex import Spy, called
from unittest import TestCase
from hamcrest import assert_that, is_
from frontend import CollectorI
from common import M1, M2, M4


class CollectorServantTests(TestCase):
    """
    These are NOT remote tests. We directly instantiate servants here.
    """
    def test_order_1_block_1x1(self):
        # given
        cb_spy = Spy()
        collector = CollectorI( processors_order=1, ncols=1, nrows=1, cb=cb_spy )

        # when
        M = M1(1)
        collector.inject(0, M)

        # then
        assert_that(cb_spy.ice_response, called().async(timeout=1).with_args( M ))

    def test_order_4_blocks_1x1(self):
        # given
        cb_spy = Spy()
        collector = CollectorI( processors_order=2, ncols=2, nrows=2, cb=cb_spy )

        collector.inject(0, M1(1))
        collector.inject(1, M1(2))
        collector.inject(2, M1(3))

        # when
        collector.inject(3, M1(4))

        # then
        assert_that(cb_spy.ice_response, called().async(timeout=1).with_args( M2(1, 2,
                                                                              3, 4)) )

    def test_order_4_blocks_2x2(self):
        # given
        cb_spy = Spy()
        collector = CollectorI( processors_order=2, ncols=4, nrows=4, cb=cb_spy )
        collector.inject(0, M2(1, 2,
                               5, 6))
        collector.inject(1, M2(3, 4,
                               7, 8))
        collector.inject(2, M2(9, 10,
                               13, 14))

        # when
        collector.inject(3, M2(11, 12,
                               15, 16))

        # then
        expected = M4(1,  2,  3,  4,
                      5,  6,  7,  8,
                      9, 10, 11, 12,
                     13, 14, 15, 16)

        assert_that(cb_spy.ice_response, called().async(timeout=1).with_args( expected ) ) 

    def test_order_4_blocks_1x1_with_missing_blocks(self):
        # given
        cb_spy = Spy()
        collector = CollectorI( processors_order=2, ncols=2, nrows=2, cb=cb_spy )
        collector.inject(0, M1(1))
        collector.inject(1, M1(2))
        # block (1,0) never injected

        # when
        collector.inject(3, M1(4))

        # then
        assert_that(cb_spy.ice_response, not called().async(timeout=1) )
