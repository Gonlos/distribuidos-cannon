# -*- mode:python; coding:utf-8; tab-width:4 -*-

from unittest import TestCase

from doublex import assert_that, Spy, called, ANY_ARG

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))
import Cannon

from processor import ProcessorI

from collector import CollectorI

from common import M1, M2, M3


class ProcessorServantTests(TestCase):
    """
    These are NOT remote tests. We directly instantiate servants here.
    """
    def test_processors_1x1_block(self):
        # given
        collector = Spy( CollectorI )
        P0 = ProcessorI( 0, 1, collector )

        A = M1(2)
        B = M1(5)
        C = M1(10)

        # when
        P0.injectA(A, 0)
        P0.injectB(B, 0)

        # then
        assert_that(collector.inject, called().async(timeout=1).with_args(0, C, ANY_ARG))

    def test_processors_2x2_block(self):
        # given
        collector = Spy( CollectorI )
        P0 = ProcessorI( 0, 1, collector )
        
        A = M2(1, 2,
               3, 4)
        B = M2(5, 6,
               7, 8)
        C = M2(19, 22,
               43, 50)

        # when
        P0.injectA(A, 0)
        P0.injectB(B, 0)

        # then
        assert_that(collector.inject, called().async(timeout=1).with_args(0, C, ANY_ARG))

    def test_processors_3x3_block(self):
        # given
        collector = Spy( CollectorI )
        P0 = ProcessorI( 0, 1, collector )

        A = M3(5, 6, 0,
               4, 2, 0,
               2, 3, 0)
        B = M3(3, 5, 7,
               6, 2, 8,
               0, 0, 0)
        C = M3(51, 37, 83,
               24, 24, 44,
               24, 16, 38)

        # when
        P0.injectA(A, 0)
        P0.injectB(B, 0)

        # then
        assert_that(collector.inject, called().async(timeout=1).with_args(0, C, ANY_ARG))

    def test_processor_free_resource(self):
        # given
        P_above = Spy( ProcessorI( 1, 2, None ) )
        P_left = Spy( ProcessorI( 2, 2, None ) )
        P0 = ProcessorI( 3, 2, None )
        P0.setProcessors( P_left, P_above )

        A = M1(2)
        B = M1(5)

        # when
        P0.injectA(A, 0)
        P0.injectB(B, 0)

        # then
        assert_that(P_above.injectB, called().async(timeout=1).with_args(B, 1))
        assert_that(P_left.injectA, called().async(timeout=1).with_args(A, 1))
