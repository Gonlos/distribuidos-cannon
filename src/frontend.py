#!/usr/bin/python
import sys

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))

import Cannon
from collector import CollectorI
from processorscontainer import ProcessorsContainerI
import threading
import matrix_utils

import log

class FrontendI(Cannon.Frontend):
    def __init__( self, processors_order, main_factory = None ):
        self.processors_order = processors_order
        self.main_factory = main_factory

    def create_collector( self, ncols, nrows, current, cb ):
        servant = CollectorI( self.processors_order, ncols, nrows, cb )
        proxy = current.adapter.addWithUUID( servant )
        return Cannon.CollectorPrx.uncheckedCast( proxy )
    
    def create_processors( self, current, collector ):
        servant = ProcessorsContainerI( self.processors_order**2 )
        proxy = current.adapter.addWithUUID( servant )
        container_proxy = Cannon.ProcessorsContainerPrx.uncheckedCast( proxy )

        self.main_factory.begin_make( self.processors_order, collector, container_proxy )

        processors = [None] * self.processors_order**2

        for index, processor in servant.iterate():
            processors[ index ] = processor

        return processors 

    def init_processors_ring( self, processors ):
        for iOrder in xrange( self.processors_order ):
            for jOrder in xrange( self.processors_order ):
                processor = processors[ iOrder * self.processors_order + jOrder ]
                
                processor_above = processors[ (iOrder-1) * self.processors_order + jOrder ]
                processor_left = processors[ iOrder * self.processors_order + ((jOrder-1) % self.processors_order) ]
                
                processor.begin_setProcessors( processor_left, processor_above )

    def load_processors(self, processors, a, b):
        for id, partial_a, partial_b in matrix_utils.matrix_split_and_shift( a, b, self.processors_order ):
            processor = processors[ id ]
            processor.begin_injectA( partial_a, 0 )
            processor.begin_injectB( partial_b, 0 )

    def multiply_async(self, cb, a, b, current=None ):
        ncols = b.ncols
        nrows = len( a.data ) // a.ncols

        log.print_message( "Creando el colector" )
        collector = self.create_collector( ncols, nrows, current, cb )
        log.print_message( "Creando los procesadores" )
        processors = self.create_processors( current, collector )
        log.print_message( "Definiendo el anillo de procesadores" )
        self.init_processors_ring( processors )
        log.print_message( "Enviando las matrices troceadas a los procesadores" )
        self.load_processors( processors, a, b )

class ServerFrontend(Ice.Application):
    def run(self, args ):
        order = int( args[1] )

        broker = self.communicator()

        main_factory_proxy = broker.stringToProxy( "ProcessorMainFactory" )
        main_factory = Cannon.ProcessorMainFactoryPrx.checkedCast( main_factory_proxy )

        servant = FrontendI( order, main_factory )
       
        adapter = broker.createObjectAdapter("FrontendAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("Cannon"))

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

if __name__ == '__main__':
    app = ServerFrontend()
    sys.exit(app.main(sys.argv))
