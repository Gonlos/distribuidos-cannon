#!/usr/bin/python
import sys

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))

import Cannon

import Queue 
import threading
import matrix_utils

NUM_JOBS_SUM = 2

class MatrixPartial:
    def __init__( self, matrix, index ):
        self.matrix = matrix
        self.index = index

class MatrixResult:
    def __init__( self, ncols, nrows, processors_order, cb ):
        self.__callback = cb
        self.__processors_order = processors_order
        self.__partial_result_remaining = processors_order**2
        self.__matrix = Cannon.Matrix( ncols, [0] * ( ncols * nrows ) )
        self.__lock_partial_result_remaining = threading.Lock()

    def addPartialMatrix( self, matrix_partial ):
        matrix_utils.matrix_join( self.__matrix, matrix_partial.matrix, matrix_partial.index, self.__processors_order )
        with self.__lock_partial_result_remaining:
            self.__partial_result_remaining-=1
            if self.__partial_result_remaining <= 0:
                self.__callback.ice_response( self.__matrix ) 
        
class JobPartialResult(threading.Thread):
    def __init__( self, queue_partial_result, matrix_result ):
        threading.Thread.__init__(self)
        self.__queue_partial_result = queue_partial_result
        self.__matrix_result = matrix_result

    def run( self ):
        while True:
            partial_matrix = self.__queue_partial_result.get()
            
            self.__matrix_result.addPartialMatrix( partial_matrix )
            
            self.__queue_partial_result.task_done()

class CollectorI(Cannon.Collector):
    def __init__( self, processors_order, ncols, nrows, cb = None ):
        self.__matrix_result = MatrixResult( ncols, nrows, processors_order, cb )
        
        self.__queue_partial_result = Queue.Queue()
        
        for i in xrange( NUM_JOBS_SUM ):
            job = JobPartialResult( self.__queue_partial_result, self.__matrix_result )
            job.setDaemon( True )
            job.start()
    
    def inject( self, index, m, current=None ):
        self.__queue_partial_result.put( MatrixPartial( m, index ) )

#class ServerCollector(Ice.Application):
    #def run(self, args):
        #broker = self.communicator()

        #adapter = broker.createObjectAdapter('CollectorAdapter')
        #self.__servant.proxy = adapter.addWithUUID(self.__servant)

        #adapter.activate()
        #self.shutdownOnInterrupt()
        #broker.waitForShutdown()

#if __name__ == '__main__':
#    app = ServerCollector(None)
#    sys.exit(app.main(sys.argv))
