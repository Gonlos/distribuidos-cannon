#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

REGISTRY_IP = sys.argv[2]
REGISTRY_PORT = 4061
REGISTRY_PROXY = "IceGrid/Locator -t:tcp -h {} -p {}".format( REGISTRY_IP, REGISTRY_PORT )

REGISTRY_NODE = """Ice.Default.Locator={}
IceGrid.Node.Name={}
IceGrid.Node.Data={}
IceGrid.Node.Endpoints=tcp
IceGrid.Node.CollocateRegistry=1
IceGrid.Registry.Client.Endpoints=tcp -p {}
IceGrid.Registry.Server.Endpoints=tcp
IceGrid.Registry.Internal.Endpoints=tcp
IceGrid.Registry.Data={}
IceGrid.Registry.PermissionsVerifier=IceGrid/NullPermissionsVerifier
IceGrid.Registry.AdminPermissionsVerifier=IceGrid/NullPermissionsVerifier
IceGrid.Registry.DefaultTemplates=/usr/share/doc/ice35-services/config/templates.xml

# Ice.StdOut=/tmp/db/node1/out.txt
# Ice.StdErr=/tmp/db/node1/err.txt"""

DEFAULT_NODE = """Ice.Default.Locator={}
IceGrid.Node.Name={}
IceGrid.Node.Data={}
IceGrid.Node.Endpoints=tcp

# Ice.StdOut=/tmp/db/node2/out.txt
# Ice.StdErr=/tmp/db/node2/err.txt"""

APP_PATH = "/tmp/MatrixMultiply"
NODE_NAME = "node{}"

num_nodos = int( sys.argv[1] )

f = open( "./nodes/node{}.config".format( 1 ), 'w' )
f.write( REGISTRY_NODE.format( REGISTRY_PROXY, NODE_NAME.format( 1 ), "{}/db/node{}".format( APP_PATH, 1 ), REGISTRY_PORT, "{}/db/registry".format( APP_PATH ) ) )
f.close()

for i in xrange( 2, num_nodos+1 ):
    f = open( "./nodes/node{}.config".format( i ), 'w' )
    f.write( DEFAULT_NODE.format( REGISTRY_PROXY, NODE_NAME.format( i ), "{}/db/node{}".format( APP_PATH, i ) ) )
    f.close()
