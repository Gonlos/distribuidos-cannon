#include <Ice/BuiltinSequences.ice>

module Cannon {
  exception AlreadyExists{ string key; };
  exception NoSuchKey{ string key; };

  struct Matrix {
    int ncols;
    Ice::DoubleSeq data;
    string UUID;
  };

  interface Frontend {
    ["amd"] Matrix multiply( Matrix a, Matrix b );
  };

  interface Collector {
    void inject( int index, Matrix m );
  };
  
  interface Processor {
    void setProcessors( Processor* left, Processor* above );
    void injectA( Matrix a, int step);
    void injectB( Matrix b, int step);
  };

  interface ProcessorsContainer {
    void add( int index, Processor* proc );
  };
   
  interface ProcessorFactory {
    void make( int index, int order, Collector* target, ProcessorsContainer* container );
  };

  interface ProcessorMainFactory {
    void link( string key, ProcessorFactory* proxy ) throws AlreadyExists;
    void unlink( string key ) throws NoSuchKey;
    void make( int order, Collector* target, ProcessorsContainer* container );
  };
 
};
