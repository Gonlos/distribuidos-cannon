#!/usr/bin/python
import sys

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))
import Cannon

from common import M1, M2, M3, MX

class Client(Ice.Application):
    def run(self, argv):
        frontend_processor = self.communicator().stringToProxy( argv[1] )
        frontend = Cannon.FrontendPrx.checkedCast( frontend_processor )

        if not frontend:
            raise RuntimeError('Invalid frontend proxy')

        # given

        #A = Cannon.Matrix( 3,  [1, 2, 3,
                                #4, 5, 6 ] )
        #B = Cannon.Matrix( 1,  [7,
                                #8,
                                #9 ] )
        n = int( argv[2] )
        A_last = n**2
        A = MX( n, 1 )
        B = MX( n, A_last)       
        C = frontend.multiply( A, B )

        print( "Recibida la matriz:" )
        
        for row in xrange( len(C.data) // C.ncols ):
            print( "  " + " ".join( map( str, C.data[ row * C.ncols : (row+1) * C.ncols ] ) ) )

        print("")
        sys.stdout.flush()

        return 0

if __name__ == '__main__':
    app = Client()
    sys.exit(app.main(sys.argv))
