#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))

from processor import ProcessorI
import Cannon
import log

class ProcessorFactoryI(Cannon.ProcessorFactory):
    def make( self, index, order, target, processors_container, current=None ):
        log.print_message( "Creando un procesador con indice {}".format( index ) )
        processor = ProcessorI( index, order, target )
        proxy = current.adapter.addWithUUID( processor )
        processors_container.begin_add( index, Cannon.ProcessorPrx.uncheckedCast( proxy ) )
        log.print_message( "Creando el procesador '{}''".format( proxy.ice_getIdentity().name ) )

class ServerProcessorFactory(Ice.Application):
    def run(self, args):
        broker = self.communicator()
        servant = ProcessorFactoryI()

        adapter = broker.createObjectAdapter('ProcessorFactoryAdapter')
        proxy = adapter.addWithUUID(servant)

        adapter.activate()
        
        print('{}'.format(proxy))
        sys.stdout.flush()

        
        main_factory_proxy = broker.stringToProxy( "ProcessorMainFactory" )
        #main_factory_proxy = broker.stringToProxy( "ProcessorMainFactory -t:tcp -h 127.0.0.1 -p 9991" )
        main_factory = Cannon.ProcessorMainFactoryPrx.checkedCast( main_factory_proxy )

        main_factory.link( proxy.ice_getIdentity().name , Cannon.ProcessorFactoryPrx.uncheckedCast( proxy ) )

        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

if __name__ == '__main__':
    app = ServerProcessorFactory()
    sys.exit(app.main(sys.argv))
