#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))

import Cannon
import log

class ProcessorMainFactoryI(Cannon.ProcessorMainFactory):
    def __init__( self ):
        self.__dic_processors = {}

    def link( self, key, proxy, current=None ):
        if key in self.__dic_processors:
            raise Services.AlreadyExists( key )

        self.__dic_processors[ key ] = proxy
        log.print_message( "Añadida la factoria '{}'".format( key ) )

    def unlink( self, key, current=None ):
        if not key in self.__dic_processors:
            raise Services.NoSuchKey( key )
        
        del self.__dic_processors[ key ]
        log.print_message( "Eliminada la factoria '{}'".format( key ) )

    def make( self, order, target, processors_container, current=None ):
        n = order**2-1
        log.print_message( "Creando {} procesadores".format( n+1 ) )
        
        while True:
            for processor_factory in self.__dic_processors.itervalues():
                processor_factory.begin_make( n, order, target, processors_container )
                log.print_message( "Creando el procesador {}".format( n ) )

                n-=1
                if n < 0:
                    log.print_message( "Creados los procesadores" )
                    return

class ServerProcessorMainFactory(Ice.Application):
    def run(self, args):
        broker = self.communicator()
        servant = ProcessorMainFactoryI()

        adapter = broker.createObjectAdapter('ProcessorMainFactoryAdapter')
        proxy = adapter.add(servant, broker.stringToIdentity( "ProcessorMainFactory" ))

        print('{}'.format(proxy))
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

if __name__ == '__main__':
    app = ServerProcessorMainFactory()
    sys.exit(app.main(sys.argv))
