#!/usr/bin/python
import time
import sys

def print_message( message ):        
    print( "[{}] {}".format( time.asctime(),message ) )
    sys.stdout.flush()
