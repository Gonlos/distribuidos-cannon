#!/usr/bin/python
import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))

import Cannon

def matrix_split_and_shift( a, b, processors_order ):
    aHeight = len( a.data ) // a.ncols
    bHeight = len( b.data ) // b.ncols

    matrix_order = max( a.ncols, aHeight, b.ncols ) // processors_order
    if b.ncols % processors_order != 0:
        matrix_order += 1
    
    for iOrder in xrange( processors_order ):
        for jOrder in xrange( processors_order ):                
            partial_a = Cannon.Matrix( matrix_order, [0] * matrix_order**2 )
            partial_b = Cannon.Matrix( matrix_order, [0] * matrix_order**2 )

            iOrderVirtual = (iOrder+jOrder) % processors_order
            jOrderVirtual = (jOrder+iOrder) % processors_order

            for i in xrange( min( matrix_order, aHeight - iOrder * matrix_order ) ):
                for j in xrange( min( matrix_order, a.ncols - jOrderVirtual * matrix_order ) ):
                    index_partial = i * matrix_order + j 
                    index_adata = iOrder * matrix_order * a.ncols + jOrderVirtual * matrix_order + i * a.ncols + j
                    
                    partial_a.data[ index_partial ] = a.data[ index_adata ]
                    
            for i in xrange( min( matrix_order, bHeight - iOrderVirtual * matrix_order ) ):
                for j in xrange( min( matrix_order, b.ncols - jOrder * matrix_order ) ):
                    index_partial = i * matrix_order + j
                    index_bdata = iOrderVirtual * matrix_order * b.ncols + jOrder * matrix_order + i * b.ncols + j

                    partial_b.data[ index_partial ] = b.data[ index_bdata ]

            yield ( iOrder * processors_order + jOrder, partial_a, partial_b )
    
def matrix_join( matrix_result, matrix_cut, matrix_cut_index, processors_order ):
    matrix_cut_nrows = len( matrix_cut.data ) // matrix_cut.ncols

    iOrder = ( matrix_cut_index // processors_order ) * matrix_cut_nrows
    jOrder = ( matrix_cut_index % processors_order ) * matrix_cut.ncols

    matrix_result_nrows = len( matrix_result.data ) // matrix_result.ncols
    
    for i in xrange( min( matrix_cut_nrows, matrix_result_nrows - iOrder ) ):
        for j in xrange( min( matrix_cut.ncols, matrix_result.ncols - jOrder ) ):
            index_result = iOrder * matrix_result.ncols + jOrder + i * matrix_result.ncols + j
            index_cut = i * matrix_cut.ncols + j

            matrix_result.data[ index_result ] = matrix_cut.data[ index_cut ]

def matrix_sum( a, b ): 
    return map( lambda x, y: x+y, a.data, b.data )
        
def matrix_multiply( a, b ): 
    order = a.ncols
    height = len( a.data ) // a.ncols
    width = b.ncols
    c = Cannon.Matrix( width, [0] * height * width )

    for i in xrange( height ):
        for k in xrange( order ):
            for j in xrange( width ):
                c.data[ i * width + j ] += a.data[ i * order + k ] * b.data[ k * width + j ]

    return c
