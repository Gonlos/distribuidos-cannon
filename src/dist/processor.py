#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Queue
import threading

import Ice
Ice.loadSlice('-I {} cannon.ice'.format(Ice.getSliceDir()))

import Cannon

import matrix_utils
import log

NUM_JOBS = 2

class MatrixPair:
    def __init__( self, step ):
        self.a = None
        self.b = None
        self.step = step

    def is_ready( self ):
        return ( self.a != None ) and ( self.b != None )

class JobMatrixMultiply(threading.Thread):
    def __init__( self, queue_matrix_pair, queue_partial_result, queue_resource ):
        threading.Thread.__init__(self)
        self.__queue_matrix_pair = queue_matrix_pair
        self.__queue_partial_result = queue_partial_result
        self.__queue_resource = queue_resource

    def run( self ):
        while True:
            # obtenemos la matrix
            matrix_pair = self.__queue_matrix_pair.get()

            # la multiplicamos
            log.print_message( "Multiplicando el par de matrices {}".format( matrix_pair.step ) )
            matrix_multiplied = matrix_utils.matrix_multiply( matrix_pair.a, matrix_pair.b )
            log.print_message( "Multiplicado el par de matrices {}".format( matrix_pair.step ) )
            
            # metemos las matrices usadas y generadas en las colas correspondientes
            self.__queue_partial_result.put( matrix_multiplied )
            self.__queue_resource.put( matrix_pair )

            # decimos que ya hemos terminado una tarea
            self.__queue_matrix_pair.task_done()

class JobPassResource(threading.Thread):
    def __init__( self, queue_resource, processors_order ):
        threading.Thread.__init__(self)
        self.__lock = threading.Lock()
        self.__lock.acquire( False )
        self.__processors_order = processors_order
        self.__queue_resource = queue_resource
        self.__left_processor = None
        self.__above_processor = None

    def setProcessors( self, left, above ):
        self.__left_processor = left
        self.__above_processor = above
        self.__lock.release()

    def run( self ):
        # esperamos hasta que tenemos los procesadores superior e izquierdo
        log.print_message( "Esperando a que se definan los procesadores superior e izquierdo" )
        self.__lock.acquire()
        log.print_message( "Se han definido los procesadores superior e izquierdo" )
        while True:
            matrix_pair = self.__queue_resource.get()

            # si aún no ha realizado una vuelta al anillo enviamos las matrices
            log.print_message( "Recibida el par de matrices {}".format( matrix_pair.step ) )
            matrix_pair.step+=1
            if matrix_pair.step < self.__processors_order:
                log.print_message( "Enviando el par de matrices {}".format( matrix_pair.step ) )
                self.__left_processor.begin_injectA( matrix_pair.a, matrix_pair.step )
                self.__above_processor.begin_injectB( matrix_pair.b, matrix_pair.step )
            
            self.__queue_resource.task_done()

class JobPartialResult(threading.Thread):
    def __init__( self, queue_partial_result, num_sums, index, target ):
        threading.Thread.__init__(self)
        self.__queue_partial_result = queue_partial_result
        self.__num_sums = num_sums
        self.__index = index
        self.__target = target
        self.__partial_result = None

    def sum( self, matrix ):
        if self.__partial_result == None:
            self.__partial_result = matrix
        else:
            self.__partial_result.data = matrix_utils.matrix_sum( self.__partial_result, matrix )

    def run( self ):
        for i in xrange( self.__num_sums ):
            matrix = self.__queue_partial_result.get()

            log.print_message( "Sumando la matriz parcial {}".format( i ) )
            self.sum( matrix )

            self.__queue_partial_result.task_done()

        log.print_message( "Enviando el resultado al collector" )
        self.__target.inject( self.__index, self.__partial_result )

class ProcessorI(Cannon.Processor):
    def __init__( self, index, order, target ):
        self.__buffer = {}
        self.__processors_order = order

        self.__queue_matrix_pair = Queue.Queue()
        queue_partial_result = Queue.Queue()
        queue_resource = Queue.Queue()

        job = JobPartialResult( queue_partial_result, order, index, target )
        job.setDaemon( True )
        job.start()

        self.__job_resources = JobPassResource( queue_resource, order )
        self.__job_resources.setDaemon( True )
        self.__job_resources.start()

        for i in xrange( NUM_JOBS ):
            job = JobMatrixMultiply( self.__queue_matrix_pair, queue_partial_result, queue_resource )
            job.setDaemon( True )
            job.start()

    def __get_matrix_pair( self, step ):
        if step in self.__buffer:
            matrix_pair = self.__buffer[ step ]
        else:
            matrix_pair = MatrixPair( step )
            self.__buffer[ step ] = matrix_pair

        return matrix_pair

    def setProcessors( self, left, above, current=None ):
        self.__job_resources.setProcessors( left, above )

    def injectA( self, a, step, current=None ):
        log.print_message( "Recibida la matriz A con el step:{}".format( step ) )
        matrix_pair = self.__get_matrix_pair( step )
        matrix_pair.a = a

        if matrix_pair.is_ready():
            log.print_message( "Procesando el par de matrices con step:{}".format( step ) )
            self.__queue_matrix_pair.put( self.__buffer.pop( step ) )
        
    def injectB( self, b, step, current=None ):
        log.print_message( "Recibida la matriz B con el step:{}".format( step ) )
        matrix_pair = self.__get_matrix_pair( step )
        matrix_pair.b = b

        if matrix_pair.is_ready():
            log.print_message( "Procesando el par de matrices con step:{}".format( step ) )
            self.__queue_matrix_pair.put( self.__buffer.pop( step ) )
        
class ServerProcessor(Ice.Application):
    def run(self, args):
        broker = self.communicator()
        servant = ProcessorI()

        adapter = broker.createObjectAdapter('ProcessorAdapter')
        proxy = adapter.addWithUUID(servant)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0

if __name__ == '__main__':
    app = ServerProcessor()
    sys.exit(app.main(sys.argv))
